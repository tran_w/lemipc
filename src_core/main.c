/*
** main.c for main in /home/naina_r//lemipc
** 
** Made by richard naina
** Login   <naina_r@epitech.net>
** 
** Started on  Thu Mar 28 18:28:45 2013 richard naina
** Last update Sun Mar 31 09:57:39 2013 richard naina
*/

#include	<time.h>
#include	<stdio.h>
#include	<stdlib.h>
#include	"lemipc.h"

int		main(int ac, char **av)
{
  t_lemipc	lemipc;
  t_pos		pos;

  if (ac != 2)
    {
      printf("[USAGE] : ./lemipc <team_number>\n");
      return (EXIT_FAILURE);
    }
  srand(time(NULL));
  if (init_lemipc(&lemipc) == R_FAILURE)
    return (EXIT_FAILURE);
  init_pos(&lemipc, &pos, atoi(av[1]));
  run(&lemipc, &pos, atoi(av[1]));
  return (EXIT_SUCCESS);
}
