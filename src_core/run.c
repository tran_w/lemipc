/*
** run.c for run in /home/naina_r//lemipc
** 
** Made by richard naina
** Login   <naina_r@epitech.net>
** 
** Started on  Fri Mar 29 17:05:14 2013 richard naina
** Last update Sun Mar 31 10:00:18 2013 richard naina
*/

#include		<stdio.h>
#include		<unistd.h>
#include		<sys/sem.h>
#include		"lemipc.h"

void			move(t_lemipc *lemipc, t_pos *pos, int team_id)
{
  struct sembuf		sops;

  if (semctl(lemipc->_semid, 0, GETVAL) == 1)
    {
      sops.sem_num = 0;
      sops.sem_op = -1;
      sops.sem_flg = 0;
      semop(lemipc->_semid, &sops, 1);
      printf("map[%d][%d] = %d; ", pos->y, pos->x, lemipc->_map[pos->y * XSIZE + pos->x]);
      lemipc->_map[pos->y * XSIZE + pos->x] = 0;
      printf("map[%d][%d] = %d; ", pos->y, pos->x, lemipc->_map[pos->y * XSIZE + pos->x]);
      if (pos->x + 1 < XSIZE && lemipc->_map[pos->y * XSIZE + pos->x + 1] == 0)
	pos->x += 1;
      else
	pos->x -= 1;
      lemipc->_map[pos->y * XSIZE + pos->x] = team_id;
      printf("map[%d][%d] = %d\n", pos->y, pos->x, lemipc->_map[pos->y * XSIZE + pos->x]);
      sops.sem_num = 0;
      sops.sem_op = 1;
      sops.sem_flg = 0;
      semop(lemipc->_semid, &sops, 1);
    }
}

void			run(t_lemipc *lemipc, t_pos *pos, int team_id)
{
  while (IS_TRUE)
    {
      move(lemipc, pos, team_id);
      usleep(100000);
      //      is_finished(lemipc->_map);
    }
}
