/*
** check_map.c for check in /home/naina_r//lemipc
** 
** Made by richard naina
** Login   <naina_r@epitech.net>
** 
** Started on  Fri Mar 29 17:00:21 2013 richard naina
** Last update Sat Mar 30 17:00:41 2013 richard naina
*/

#include		<stdio.h>
#include		<stdlib.h>
#include		"lemipc.h"

static int		_check_map(int *map)
{
  int			i;
  int			j;

  i = 0;
  while (i < YSIZE)
    {
      j = 0;
      while (j < XSIZE)
	if (map[i * XSIZE + j++] == 0)
	  return (0);
      ++i;
    }
  return (-1);
}

void			init_pos(t_lemipc *lemipc, t_pos *pos, int team_number)
{
  int			check;

  check = -1;
  if (_check_map(lemipc->_map) == -1)
    return ;
  while (check != 0)
    {
      pos->x = rand() % XSIZE;
      pos->y = rand() % YSIZE;
      if (lemipc->_map[pos->y * XSIZE + pos->x] == 0)
	{
	  lemipc->_map[pos->y * XSIZE + pos->x] = team_number;
	  check = 0;
	  printf("lemipc->_map[%d][%d] = %d\n", pos->y, pos->x, lemipc->_map[pos->y * XSIZE + pos->x]);
	}
    }
}
