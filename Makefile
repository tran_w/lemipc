##
## Makefile for make in /home/naina_r//lemipc
## 
## Made by richard naina
## Login   <naina_r@epitech.net>
## 
## Started on  Thu Mar 28 18:27:05 2013 richard naina
## Last update Sun Mar 31 00:55:10 2013 tran_w
##

NAME1	=	lemipc

NAME2	=	viewer

INCDIR	=	./inc

COREDIR	=	./src_core

VIEWDIR	=	./src_viewer

SRCS1	=	$(COREDIR)/main.c		\
		$(COREDIR)/init_lemipc.c	\
		$(COREDIR)/check_map.c		\
		$(COREDIR)/run.c

SRCS2	=	$(VIEWDIR)/main.c		\
		$(VIEWDIR)/init_lemipc.c	\
		$(VIEWDIR)/display.c

OBJS1	=	$(SRCS1:.c=.o)

OBJS2	=	$(SRCS2:.c=.o)

CC	=	gcc

LDFLAGS	+=	-lSDL

CFLAGS	+=	-W -Wall -Wextra -I$(INCDIR)

all	:	$(NAME1) $(NAME2)

$(NAME1):	$(OBJS1)
		$(CC) -o $(NAME1) $(OBJS1)

$(NAME2):	$(OBJS2)
		$(CC) -o $(NAME2) $(OBJS2) $(LDFLAGS)

clean	:
		$(RM) $(OBJS1)
		$(RM) $(OBJS2)
		$(RM) $(COREDIR)/*~
		$(RM) $(VIEWDIR)/*~
		$(RM) $(INCDIR)/*~
		$(RM) $(COREDIR)/\#*#
		$(RM) $(VIEWDIR)/\#*#
		$(RM) $(INCDIR)/\#*#

fclean	:	clean
		$(RM) $(NAME1) $(NAME2)

re	:	fclean all

.PHONY	:	all fclean clean re
