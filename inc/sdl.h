/*
** sdl.h for sdl in /home/naina_r//lemipc
** 
** Made by richard naina
** Login   <naina_r@epitech.net>
** 
** Started on  Sat Mar 30 17:23:12 2013 richard naina
** Last update Sun Mar 31 09:41:29 2013 richard naina
*/

#ifndef			SDL_H_
# define		SDL_H_

# include		<SDL/SDL.h>

# define		XWIN		(800)
# define		YWIN		(800)

# define		MAX_PATH_SIZE	(512)

# define		EVENT_EXIT	(1)

# define		R_FAILURE	(-1)
# define		R_SUCCESS	(0)
# define		IS_TRUE		(42)

typedef struct		s_image
{
  int			x;
  int			y;
  int			r;
  int			g;
  int			b;
}			t_image;

typedef struct		s_graph
{
  SDL_Surface		*_screen;
  SDL_Surface		*_background;
  SDL_Surface		*_character;
  t_image		_image;
}			t_graph;

int			getEvent();
void			display(t_lemipc *lemipc);

#endif			/* !SDL_H_ */
