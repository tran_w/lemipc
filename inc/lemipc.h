/*
** lemipc.h for lemipc in /home/naina_r//lemipc
** 
** Made by richard naina
** Login   <naina_r@epitech.net>
** 
** Started on  Thu Mar 28 18:29:08 2013 richard naina
** Last update Sun Mar 31 01:16:11 2013 tran_w
*/

#ifndef			LEMIPC_H_
# define		LEMIPC_H_

# define		XSIZE		(20)
# define		YSIZE		(20)
# define		SHMSIZE		((XSIZE + 1) * (YSIZE + 1))

# define		MAX_PATH_SIZE	(512)

# define		R_FAILURE	(-1)
# define		R_SUCCESS	(0)
# define		IS_TRUE		(42)

typedef struct		s_pos
{
  int			x;
  int			y;
}			t_pos;

typedef struct		s_lemipc
{
  int			*_map;
  int			_shmid;
  int			_semid;
}			t_lemipc;

int			init_lemipc(t_lemipc *lemipc);
void			init_pos(t_lemipc *lemipc, t_pos *pos, int team_number);
void			run(t_lemipc *lemipc, t_pos *pos, int team_id);

#endif			/* !LEMIPC_H_ */
