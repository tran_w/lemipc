/*
** main.c for main in /home/naina_r//lemipc
** 
** Made by richard naina
** Login   <naina_r@epitech.net>
** 
** Started on  Thu Mar 28 18:28:45 2013 richard naina
** Last update Sun Mar 31 09:06:14 2013 richard naina
*/

#include	<stdio.h>
#include	<stdlib.h>
#include	"lemipc.h"
#include	"sdl.h"

int			getEvent()
{
  SDL_Event		event;

  SDL_PollEvent(&event);
  if (event.type == SDL_KEYDOWN)
    if (event.key.keysym.sym == SDLK_ESCAPE)
      return (EVENT_EXIT);
  if (event.type == SDL_QUIT)
    return (EVENT_EXIT);
  return (R_SUCCESS);
}

void		loop(t_lemipc *lemipc)
{
  while (IS_TRUE)
    {
      if (getEvent() == EVENT_EXIT)
	return ;
      display(lemipc);
    }
}

int		main()
{
  t_lemipc	lemipc;

  if (init_lemipc(&lemipc) == R_FAILURE)
    return (EXIT_FAILURE);
  loop(&lemipc);
  return (EXIT_SUCCESS);
}
