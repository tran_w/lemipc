/*
** init_lemipc.c for init in /home/naina_r//lemipc
** 
** Made by richard naina
** Login   <naina_r@epitech.net>
** 
** Started on  Thu Mar 28 18:38:17 2013 richard naina
** Last update Sun Mar 31 01:16:45 2013 tran_w
*/

#include		<unistd.h>
#include		<stdlib.h>
#include		<stdio.h>
#include		<ctype.h>
#include		<stdlib.h>
#include		<sys/sem.h>
#include		<sys/types.h>
#include		<sys/ipc.h>
#include		<sys/shm.h>
#include		"lemipc.h"

static void		_init_map(int *map)
{
  int			i;
  int			j;

  i = 0;
  while (i < YSIZE)
    {
      j = 0;
      while (j < XSIZE)
	map[i * XSIZE + j++] = 0;
      ++i;
    }
}

void			show_map(t_lemipc *lemipc)
{
  int			i;
  int			j;

  for (i = 0; i < XSIZE; ++i)
    {
      for (j = 0; j < YSIZE; ++j)
	printf("map[%d][%d] = %d; ", i, j, lemipc->_map[i * XSIZE + j]);
      printf("\n");
    }
}

static int		_init_shm(t_lemipc *lemipc, key_t key)
{
  if ((lemipc->_shmid = shmget(key, SHMSIZE, SHM_R | SHM_W)) == R_FAILURE)
    {
      if ((lemipc->_shmid = shmget(key, SHMSIZE,
				   IPC_CREAT | SHM_R | SHM_W)) == R_FAILURE)
	return (R_FAILURE);
      lemipc->_map = (int *)shmat(lemipc->_shmid, NULL, SHM_R | SHM_W);
      _init_map(lemipc->_map);
      shmdt(lemipc->_map);
    }
  lemipc->_map = (int *)shmat(lemipc->_shmid, NULL, SHM_R | SHM_W);
  return (R_SUCCESS);
}

static int		_init_sem(t_lemipc *lemipc, key_t key)
{
  if ((lemipc->_semid = semget(key, 1, SHM_R | SHM_W)) == R_FAILURE)
    {
      if ((lemipc->_semid = semget(key, 1, IPC_CREAT | SHM_R | SHM_W)) == R_FAILURE)
	return (R_FAILURE);
      semctl(lemipc->_semid, 0, SETVAL, 1);
    }
  return (R_SUCCESS);
}

int			init_lemipc(t_lemipc *lemipc)
{
  key_t			key;
  char			path[MAX_PATH_SIZE];

  getcwd(path, sizeof(path));
  if ((key = ftok(path, 0)) == R_FAILURE)
    return (R_FAILURE);
  if (_init_shm(lemipc, key) == R_FAILURE)
    return (R_FAILURE);
  if (_init_sem(lemipc, key) == R_FAILURE)
    return (R_FAILURE);
  return (R_SUCCESS);
}
