/*
** display.c for display in /home/naina_r//lemipc
** 
** Made by richard naina
** Login   <naina_r@epitech.net>
** 
** Started on  Sat Mar 30 17:07:27 2013 richard naina
** Last update Sun Mar 31 09:54:06 2013 richard naina
*/

#include		"lemipc.h"
#include		"sdl.h"

void			set_character(int team_id, t_graph *graph)
{
  if (team_id == 1)
    {
      graph->_character = SDL_LoadBMP("./images/Sasuke.bmp");
      graph->_image.r = 144;
      graph->_image.g = 176;
      graph->_image.b = 216;
    }
  else if (team_id == 2)
    {
      graph->_character = SDL_LoadBMP("./images/naruto.bmp");
      graph->_image.r = 51;
      graph->_image.g = 204;
      graph->_image.b = 255;
    }
  graph->_image.x = 54;
  graph->_image.y = 63;
}

void			set_image(int team_id, t_graph *graph, int y, int x)
{
  SDL_Rect      src;
  SDL_Rect      dst;

  set_character(team_id, graph);
  src.x = 0;
  src.y = 0;
  src.w = graph->_image.x;
  src.h = graph->_image.y;
  if (team_id == 0)
    {
      dst.x = x;
      dst.y = y;
    }
  else
    {
      dst.x = (x + 1) * 32;
      dst.y = y * 32;
    }
  dst.w = graph->_image.x;
  dst.h = graph->_image.y;
  SDL_SetColorKey(graph->_character, SDL_SRCCOLORKEY,
                  SDL_MapRGB(((SDL_Surface *)(graph->_character))->format,
                             graph->_image.r, graph->_image.g, graph->_image.b));
  SDL_BlitSurface(graph->_character, &src, graph->_screen, &dst);
}

void			set_background(t_graph *graph)
{
  int			x;
  int			y;

  y = 0;
  graph->_character = SDL_LoadBMP("./images/floor.bmp");
  graph->_image.x = 32;
  graph->_image.y = 32;
  graph->_image.r = 0;
  graph->_image.g = 0;
  graph->_image.b = 0;
  while (y < YWIN * 32)
    {
      x = 0;
      while (x < XWIN * 32)
	{
	  set_image(0, graph, x, y);
	 x += 32;
	}
      y += 32;
    }
}

void			put_image(t_graph *graph, t_lemipc *lemipc)
{
  int			y;
  int			x;

  y = 0;
  while (y < YSIZE)
    {
      x = 0;
      while (x < XSIZE)
	{
	  if (lemipc->_map[y * YSIZE + x] != 0)
	    set_image(lemipc->_map[y * YSIZE + x], graph, y, x);
	  ++x;
	}
      ++y;
    }
}

void			display(t_lemipc *lemipc)
{
  t_graph		graph;

  SDL_Init(SDL_INIT_EVERYTHING);
  graph._screen = SDL_SetVideoMode(XWIN, YWIN, 32, SDL_SWSURFACE);
  set_background(&graph);
  put_image(&graph, lemipc);
  SDL_Flip(graph._screen);
  SDL_Delay(10);
}
